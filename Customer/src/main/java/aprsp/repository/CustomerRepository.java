package aprsp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import aprsp.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

	Optional<Customer> findByUsername(String username);
	
	@Query(value = "select nextval('customer_id_seq')", nativeQuery = true)
	Integer getGeneratedId();
}
