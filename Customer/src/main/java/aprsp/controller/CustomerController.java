package aprsp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import aprsp.model.Customer;
import aprsp.service.CustomerService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class CustomerController {

	private CustomerService customerService;
	
	@Autowired
	public CustomerController(CustomerService customerService) {
		this.customerService = customerService;
	}
	
	@ApiOperation(value="Retruns list of customers")
	@GetMapping("/customer")
	public List<Customer> getCustomers(){
		return customerService.findAll();
	}
	
	@ApiOperation(value="Returns customer by id")
	@GetMapping("/customer/{id}")
	public ResponseEntity<Customer> getCustomer(@PathVariable("id") Integer id) {
		Customer customer = customerService.findOne(id);
		if(customer != null)
			return new ResponseEntity<>(customer, HttpStatus.OK);
		return new ResponseEntity<Customer>(HttpStatus.NO_CONTENT);
	}
	
	@ApiOperation(value="Returns customer by username")
	@GetMapping("/customerByUsername/{username}")
	public ResponseEntity<Customer> getCustomerByUsername(@PathVariable("username") String username){
		Customer customer = customerService.findByUsername(username);
		if(customer != null)
			return new ResponseEntity<Customer>(customer, HttpStatus.OK);
		return new ResponseEntity<Customer>(HttpStatus.NO_CONTENT);
	}
	
	@ApiOperation(value="Inserting new customer in database")
	@PostMapping("/customer")
	public ResponseEntity<Customer> insertCustomer(@RequestBody Customer customer){
		if(customerService.findByUsername(customer.getUsername()) != null)
			return new ResponseEntity<Customer>(HttpStatus.CONFLICT);
		customerService.save(customer);
		return new ResponseEntity<Customer>(customer, HttpStatus.CREATED);
	}
	
	@ApiOperation(value="Updating customer by id")
	@PutMapping("/customer/{id}")
	public ResponseEntity<Customer> updateCustomer(@RequestBody Customer customer, @PathVariable("id") int id){
		if(customerService.findOne(id) == null)
			return new ResponseEntity<Customer>(HttpStatus.NO_CONTENT);
		customer.setId(id);
		customerService.save(customer);
		return new ResponseEntity<Customer>(customerService.findOne(id), HttpStatus.OK);
	}
	
	@ApiOperation(value="Delete customer by id")
	@DeleteMapping("/customer/{id}")
	public ResponseEntity<Void> deleteCustomer(@PathVariable("id") int id) {
		if(customerService.findOne(id) == null)
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		customerService.delete(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
}
