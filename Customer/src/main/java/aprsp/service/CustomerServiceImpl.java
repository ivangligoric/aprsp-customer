package aprsp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import aprsp.model.Customer;
import aprsp.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService{

	private CustomerRepository customerRepository;
	
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	public CustomerServiceImpl(CustomerRepository customerRepository, BCryptPasswordEncoder passwordEncoder) {
		this.customerRepository = customerRepository;
		this.passwordEncoder = passwordEncoder;
	}
	
	@Override
	public List<Customer> findAll() {
		// TODO Auto-generated method stub
		return customerRepository.findAll();
	}

	@Override
	public Customer findOne(Integer id) {
		// TODO Auto-generated method stub
		return customerRepository.findById(id).orElse(null);
	}

	@Override
	public Customer findByUsername(String username) {
		// TODO Auto-generated method stub
		return customerRepository.findByUsername(username).orElse(null);
	}

	@Override
	public Customer save(Customer customer) {
		// TODO Auto-generated method stub
		if(customer.getId() == null)			
			customer.setId(customerRepository.getGeneratedId());
		customer.setPassword(passwordEncoder.encode(customer.getPassword()));
		customerRepository.save(customer);
        return customer;
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		customerRepository.deleteById(id);
		
	}

}
