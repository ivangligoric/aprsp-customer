package aprsp.service;

import java.util.List;

import org.springframework.stereotype.Component;

import aprsp.model.Customer;

@Component
public interface CustomerService {

	List<Customer> findAll();
	
	Customer findOne(Integer id);
	
	Customer findByUsername(String username);
	
	Customer save(Customer customer);
	
	void delete(int id);
}
