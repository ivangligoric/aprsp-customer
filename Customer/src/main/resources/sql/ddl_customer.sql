DROP TABLE IF EXISTS public.customer CASCADE;

create table public.customer(
	id serial primary key,
	name varchar(128),
	username varchar(128) unique not null,
	password varchar(255) not null,
	phone_number varchar(13) not null,
	email varchar(64) not null
	);

create index pk_customer on public.customer (id);

create index pk_customer_username on public.customer(username);